function z = sigmoid (x)
  
  z = 1 ./ (1 + exp(-x));
  
endfunction
